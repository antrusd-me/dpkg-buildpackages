Format: 3.0 (quilt)
Source: scrcpy
Binary: scrcpy, scrcpy-server
Architecture: any all
Version: 2.4-1
Maintainer: Yangfl <mmyangfl@gmail.com>
Homepage: https://github.com/Genymobile/scrcpy
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/yangfl-guest/scrcpy
Vcs-Git: https://salsa.debian.org/yangfl-guest/scrcpy.git
Testsuite: autopkgtest
Testsuite-Triggers: @builddeps@
Build-Depends: debhelper-compat (= 13)
Build-Depends-Arch: meson, ninja-build, pkg-config, libavcodec-dev, libavdevice-dev, libavformat-dev, libavutil-dev, libswresample-dev, libsdl2-dev, libusb-1.0-0-dev
Build-Depends-Indep: wget
Package-List:
 scrcpy deb net optional arch=any
 scrcpy-server deb net optional arch=all
Checksums-Sha1:
 31921d6223f4ee2237fcd46b44aab67ff395e532 413388 scrcpy_2.4.orig.tar.gz
 4fffebcc4da957de9e517f1652f578f5dee554a7 2904 scrcpy_2.4-1.debian.tar.xz
Checksums-Sha256:
 f69d4a1959b595453230eb77e73791ea979a6afd892dc79ba5c7fd0ecea78a1d 413388 scrcpy_2.4.orig.tar.gz
 115f50fe634d1c4e876819fb5e7d49a531d9645a7eded55ebff1242ed1de9bf6 2904 scrcpy_2.4-1.debian.tar.xz
Files:
 acaace19cccfb92ae9e6b3ee9b162652 413388 scrcpy_2.4.orig.tar.gz
 570876a721a476cdf0876156a4831a08 2904 scrcpy_2.4-1.debian.tar.xz
