#!/bin/bash

set -u

_init() {
  local _s=
  local _static_bin=("yq" "rclone")

  apt -y update
  apt -y --no-install-recommends install ca-certificates gpg gpg-agent axel

  for _s in "${_static_bin[@]}"; do
    if [[ ! -x /usr/local/bin/${_s} ]]; then
      (
        set -v -x
        axel -o /usr/local/bin/${_s} https://antrusd.net/pub/bin/${_s}
        chmod +x /usr/local/bin/${_s}
      )
    fi
  done

  export RELEASE_NAME=${CI_COMMIT_TAG%%/*}
  echo RELEASE_NAME=${RELEASE_NAME}

  export PACKAGE_NAME=${CI_COMMIT_TAG##*/}
  echo PACKAGE_NAME=${PACKAGE_NAME}

  export APT_SECURITY_ENABLED=$(yq e '.apt.sources.security|select(.==~)|=true' ${RELEASE_NAME}/${PACKAGE_NAME}/build.yaml)
  echo APT_SECURITY_ENABLED=${APT_SECURITY_ENABLED}

  export ACTION_BUILD=$(yq e '.action.build|select(.==~)|=true' ${RELEASE_NAME}/${PACKAGE_NAME}/build.yaml)
  echo ACTION_BUILD=${ACTION_BUILD}

  export ACTION_PULISH=$(yq e '.action.publish|select(.==~)|=true' ${RELEASE_NAME}/${PACKAGE_NAME}/build.yaml)
  echo ACTION_PULISH=${ACTION_PULISH}
}

_renders() {
  if [[ -r ${1} ]]; then
    eval "cat <<EOF
$(<${1})
EOF
" 2> /dev/null
  fi
}

_runas_apt(){
  su -s /bin/bash -c "${@}" _apt
}

_setup_repo() {
  local _r=
  local _add_repo=( $(yq e -I=0 '.apt.sources.extras[]' ${RELEASE_NAME}/${PACKAGE_NAME}/build.yaml) )

  _renders helpers/templates/sources.list.tpl > /etc/apt/sources.list

  for _r in "${_add_repo[@]}"; do
    echo "deb https://deb.debian.org/debian ${RELEASE_NAME}-${_r} main" >> /etc/apt/sources.list
  done

  if [[ ${APT_SECURITY_ENABLED} == 'true' ]]; then
    if [[ ${RELEASE_NAME} == 'buster' ]]; then
      echo "deb https://security.debian.org/debian-security ${RELEASE_NAME}/updates main" >> /etc/apt/sources.list
    else
      echo "deb https://security.debian.org/debian-security ${RELEASE_NAME}-security main" >> /etc/apt/sources.list
    fi
  fi

  (
    set -v -x

    cat /etc/apt/sources.list
    apt -y update
    apt -y --no-install-recommends full-upgrade
    if [[ ${ACTION_BUILD} == 'true' ]]; then
      apt -y --no-install-recommends install dpkg-dev build-essential fakeroot
    fi
  )
}

_install_dependencies() {
  local _add_dep=( $(yq e -I=0 '.apt.install[]' ${RELEASE_NAME}/${PACKAGE_NAME}/build.yaml) )

  if [[ ${#_add_dep[@]} -gt 0 ]]; then
    mkdir -pv /tmp/dependencies

    for _d in "${_add_dep[@]}"; do
      if [[ "${_d}" =~ ^https?:// ]]; then
        axel -o /tmp/dependencies/ ${_d}
      else
        if [[ -r ${RELEASE_NAME}/${PACKAGE_NAME}/${_d} ]]; then
          cp -v ${RELEASE_NAME}/${PACKAGE_NAME}/${_d} /tmp/dependencies/
        fi
      fi
    done

    find /tmp/dependencies -iname '*.deb' -exec apt -y --no-install-recommends install {} +
  fi
}

_download_sources() {
  (
    set -v -x

    _sources=( $(yq e -I=0 '.sources[]' ${RELEASE_NAME}/${PACKAGE_NAME}/build.yaml) )
    _runas_apt "mkdir -pv /tmp/${PACKAGE_NAME}"

    for _s in "${_sources[@]}"; do
      if [[ "${_s}" =~ ^https?:// ]]; then
        _runas_apt "axel -o /tmp/${PACKAGE_NAME}/ ${_s}"
      else
        if [[ -r ${RELEASE_NAME}/${PACKAGE_NAME}/${_s} ]]; then
          _runas_apt "cp -v ${RELEASE_NAME}/${PACKAGE_NAME}/${_s} /tmp/${PACKAGE_NAME}/"
        fi
      fi
    done

    if [[ ${ACTION_BUILD} == 'true' ]]; then
      _runas_apt "cd /tmp/${PACKAGE_NAME} && find . -name ${PACKAGE_NAME}_*.dsc -type f -exec dpkg-source --no-check --extract {} ';'"
    fi
  )

  if [[ ${ACTION_BUILD} == 'true' ]]; then
    export DEBIANIZED_SOURCE=$(find /tmp/${PACKAGE_NAME} -maxdepth 3 -wholename "*/debian/changelog" | sed -e 's:/\w*/\w*$::')
    (
      set -v -x
      apt -y --no-install-recommends build-dep ${DEBIANIZED_SOURCE}
    )
  fi
}

_build_publish() {
  mkdir -v -m 0700 -p ${HOME}/{.gnupg,.config/rclone}
  mkdir -v -m 0644 -p /tmp/repo/{conf,db,dists,pool}
  gpg --import <(base64 -d <<< "${GPG_PUB_BASE64}") <(base64 -d <<< "${GPG_SIG_BASE64}")
  base64 -d <<< "${RCLONE_CONFIG_BASE64}" > ${HOME}/.config/rclone/rclone.conf

  (
    set -v -x
    apt -y --no-install-recommends install reprepro

    if [[ ${ACTION_BUILD} == 'true' ]]; then
      _runas_apt "cd ${DEBIANIZED_SOURCE} && dpkg-buildpackage -us -uc -rfakeroot && rm -fr ${DEBIANIZED_SOURCE}"
    fi

    if [[ ${ACTION_PULISH} == 'true' ]]; then
      pushd /tmp/repo
      rclone sync r2:debian/conf conf
      rclone sync r2:debian/db db
      rclone sync r2:debian/dists dists
      find /tmp/${PACKAGE_NAME} -maxdepth 1 -iname '*.dsc' -exec reprepro -Vb . includedsc ${RELEASE_NAME} {} +
      find /tmp/${PACKAGE_NAME} -maxdepth 1 -iname '*.deb' -exec reprepro -Vb . includedeb ${RELEASE_NAME} {} +
      find /tmp/${PACKAGE_NAME} -maxdepth 1 -iname '*.udeb' -exec reprepro -Vb . includeudeb ${RELEASE_NAME} {} +
      rclone copy . r2:debian
      popd
    fi
  )
}
